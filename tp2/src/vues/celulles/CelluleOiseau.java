package vues.celulles;

import javafx.beans.binding.Bindings;
import javafx.scene.control.ListCell;
import modele.metier.oiseaux.Oiseau;
import viewmodel.OiseauVM;
import vues.uc.UserControl_CelluleOiseau;

import java.io.IOException;

public class CelluleOiseau extends ListCell<OiseauVM> {

    private UserControl_CelluleOiseau userControl_celluleOiseau;

    @Override
    protected void updateItem(OiseauVM item, boolean empty) {
        super.updateItem(item, empty);
        if(!empty){
            try {
                userControl_celluleOiseau = new UserControl_CelluleOiseau(item);
                setGraphic(userControl_celluleOiseau);
            } catch (IOException e) {
                textProperty().bind(item.nomProperty());
            }
        }
        else{
            setGraphic(null); // pour éviter que le uc reste apparant
        }
    }
}
