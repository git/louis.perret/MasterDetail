package viewmodel;

import javafx.beans.property.*;
import modele.metier.etat.Etat;
import modele.metier.oiseaux.Oiseau;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.time.LocalDate;

public class OiseauVM implements PropertyChangeListener {

    private Oiseau modele;

    private StringProperty nom = new SimpleStringProperty();
    public String getNom() { return nom.get(); }
    public StringProperty nomProperty() { return nom; }
    public void setNom(String nom) { this.nom.set(nom); }

    private IntegerProperty age = new SimpleIntegerProperty();
    public int getAge() { return age.get(); }
    public IntegerProperty ageProperty() { return age; }
    public void setAge(int age) { this.age.set(age); }

    private StringProperty couleurAiles = new SimpleStringProperty();
    public String getCouleurAiles() { return couleurAiles.get(); }
    public StringProperty couleurAilesProperty() { return couleurAiles; }
    public void setCouleurAiles(String couleurAiles) { this.couleurAiles.set(couleurAiles); }

    private ObjectProperty<LocalDate> dateDernierRepas = new SimpleObjectProperty<>();
    public LocalDate getDateDernierRepas() { return dateDernierRepas.get(); }
    public ObjectProperty<LocalDate> dateDernierRepasProperty() { return dateDernierRepas; }
    public void setDateDernierRepas(LocalDate dateDernierRepas) { this.dateDernierRepas.set(dateDernierRepas); }

    private ObjectProperty<Etat> etat = new SimpleObjectProperty<>();
    public Etat getEtat() { return etat.get(); }
    public ObjectProperty<Etat> etatProperty() { return etat; }
    public void setEtat(Etat etat) { this.etat.set(etat); }


    public OiseauVM(Oiseau modele) {
        this.modele = modele;
        // première initialisation des propriétés
        setNom(modele.getNom());
        setAge(modele.getAge());
        setCouleurAiles(modele.getCouleurAiles());
        setEtat(modele.getEtat());
        setDateDernierRepas(modele.getDateDernierRepas());
        modele.ajouterListener(this); // écoute les modifications de son oiseau
        nom.addListener((__,___,newV) -> modele.setNom(newV));
    }

    public OiseauVM(String nom, int age, String couleurAiles){
        this(new Oiseau(nom, age, couleurAiles));
    }

    public void seNourrir(LocalDate dateDuJour) {
        modele.seNourrir(dateDuJour);
    }

    public Oiseau getModele() {
        return modele;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        switch (evt.getPropertyName()){
            case Oiseau.PROP_ETAT:
                setEtat((Etat)evt.getNewValue());
                break;
            case Oiseau.PROP_DATE_DERNIER_REPAS:
                setDateDernierRepas((LocalDate) evt.getNewValue());
                break;
        }
    }
}
