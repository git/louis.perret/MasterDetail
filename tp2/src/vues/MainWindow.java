package vues;

import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import viewmodel.ManagerOiseauVM;
import viewmodel.OiseauVM;
import vues.celulles.CelluleOiseau;

import java.io.IOException;

public class MainWindow {

    @FXML
    public Label dateDuJour;

    @FXML
    public TextField nomOiseau;

    @FXML
    public Label ageOiseau;

    @FXML
    public Label couleurAile;

    @FXML
    public Label dateDernierRepas;

    @FXML
    public Button boutonTuerOiseau;

    @FXML
    private ListView<OiseauVM> listeOiseaux;

    @FXML
    private Button ajouterOiseau;

    public static ManagerOiseauVM managerVM;

    public void initialize(){
        boutonTuerOiseau.setDisable(true);
        managerVM = new ManagerOiseauVM();
        dateDuJour.textProperty().bind(managerVM.dateDuJourProperty().asString());
        listeOiseaux.setItems(managerVM.getListeOiseaux());
        listeOiseaux.setCellFactory((__) -> new CelluleOiseau());
        listeOiseaux.getSelectionModel().selectedItemProperty().addListener((__, oldV, newV) -> {
            if(oldV != null){
                nomOiseau.textProperty().unbindBidirectional(oldV.nomProperty());
                ageOiseau.textProperty().unbind();
                couleurAile.textProperty().unbind();
                dateDernierRepas.textProperty().unbind();
                dateDernierRepas.textProperty().unbind();
                boutonTuerOiseau.setDisable(true);
            }
            if(newV != null){
                nomOiseau.textProperty().bindBidirectional(newV.nomProperty());
                ageOiseau.textProperty().bind(Bindings.concat("Age : ", newV.ageProperty().asString(), " ans"));
                couleurAile.textProperty().bind(Bindings.concat("Couleur de ses ailes : ", newV.couleurAilesProperty()));
                dateDernierRepas.textProperty().bind(Bindings.concat("Date du dernier repas : ", newV.dateDernierRepasProperty().asString()));
                boutonTuerOiseau.setDisable(false);
            }
        });
    }

    @FXML
    public void passerUnJour() {
        managerVM.incrementerJour();
    }

    @FXML
    private void ajouterOiseau() throws IOException {
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/vues/AjouterOiseau.fxml")));
        Stage secondStage = new Stage(); // stage de ma nouvelle fenêtre
        Stage stage = (Stage)ajouterOiseau.getScene().getWindow();
        secondStage.initOwner(stage); // set sa fenêtre parent
        secondStage.setScene(scene);
        secondStage.initModality(Modality.WINDOW_MODAL); // préciser le type de fenêtre qu'on veut afficher, ici une fenêtre modale
        secondStage.show();
    }

    @FXML
    public void tuerOiseau() {
        managerVM.supprimerOiseau(listeOiseaux.getSelectionModel().getSelectedItem());
    }

    @FXML
    public void fermerFenetre() throws IOException {
        managerVM.sauver();
        ((Stage)listeOiseaux.getScene().getWindow()).close();
    }
}
