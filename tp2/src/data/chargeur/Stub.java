package data.chargeur;

import modele.metier.factory.OiseauFactory;
import modele.metier.factory.SimpleOiseauFactory;
import modele.metier.oiseaux.Oiseau;

import java.util.ArrayList;
import java.util.List;

public class Stub implements Chargeur {

    @Override
    public List<Oiseau> charger(String nomFichier) {
        List<Oiseau> listeOiseaux = new ArrayList<>();
        OiseauFactory oiseauFactory = new SimpleOiseauFactory();

        listeOiseaux.add(oiseauFactory.creerOiseau("Pedro", 20, "noires"));
        listeOiseaux.add(oiseauFactory.creerOiseau("Jeremy", 5, "blanches"));
        listeOiseaux.add(oiseauFactory.creerOiseau("Louis", 14, "roses"));
        listeOiseaux.add(oiseauFactory.creerOiseau("Deku", 9, "violet"));
        listeOiseaux.add(oiseauFactory.creerOiseau("Buble", 16, "noires"));
        listeOiseaux.add(oiseauFactory.creerOiseau("Poppy", 21, "vertes"));

        return listeOiseaux;
    }
}
