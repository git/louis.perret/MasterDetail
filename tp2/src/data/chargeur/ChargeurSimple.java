package data.chargeur;

import modele.metier.oiseaux.Oiseau;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

public class ChargeurSimple implements Chargeur{

    @Override
    public List<Oiseau> charger(String nomFichier) throws IOException, ClassNotFoundException {
        List<Oiseau> oiseaux;
        try(ObjectInputStream reader = new ObjectInputStream(new FileInputStream(nomFichier))){
            oiseaux = (List<Oiseau>) reader.readObject();
        }
        return oiseaux;
    }
}
