package viewmodel;

import data.chargeur.*;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import modele.metier.ManagerOiseau;
import modele.metier.oiseaux.Oiseau;

import java.beans.IndexedPropertyChangeEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.time.LocalDate;

public class ManagerOiseauVM implements PropertyChangeListener {

    private ManagerOiseau modele;

    private ObjectProperty<LocalDate> dateDuJour = new SimpleObjectProperty<>();
    public LocalDate getDateDuJour() { return dateDuJour.get();  }
    public ObjectProperty<LocalDate> dateDuJourProperty() { return dateDuJour; }
    public void setDateDuJour(LocalDate dateDuJour) { this.dateDuJour.set(dateDuJour); }

    private ObjectProperty<OiseauVM> oiseauCourant = new SimpleObjectProperty<>();
    public OiseauVM getOiseauCourant() { return oiseauCourant.get(); }
    public ObjectProperty<OiseauVM> oiseauCourantProperty() { return oiseauCourant; }
    public void setOiseauCourant(OiseauVM oiseauCourant) { this.oiseauCourant.set(oiseauCourant); }


    private ObservableList<OiseauVM> oiseauxObs = FXCollections.observableArrayList(); // envoie notif quand un des élémentscha
    private ListProperty<OiseauVM> listeOiseaux = new SimpleListProperty<>(oiseauxObs);
    public ObservableList<OiseauVM> getListeOiseaux() { return listeOiseaux.get(); }
    public ListProperty<OiseauVM> listeOiseauxProperty() { return listeOiseaux; }
    public void setListeOiseaux(ObservableList<OiseauVM> listeOiseaux) {this.listeOiseaux.set(listeOiseaux); }

    private Chargeur chargeur;

    private Sauveur sauveur;

    public ManagerOiseauVM() { // PAS DE PARAMETRE DANS UNE VM
        modele = new ManagerOiseau();
        chargeur = new ChargeurSimple();
        sauveur = new SauveurSimple();

        try {
            modele.addOiseaux(chargeur.charger("source.bin"));
        } catch (Exception e){
        modele.addOiseaux(new Stub().charger(""));
        }

        for(Oiseau oiseau : modele.getOiseaux()){
            oiseauxObs.add(new OiseauVM(oiseau));
            //oiseauxObs.add(new OiseauVM(oiseau.getNom(), oiseau.getAge(), oiseau.getCouleurAiles())); // pb de référence si je fais ça
        }
        setDateDuJour(modele.getDateDuJour());
        modele.addListener(this);
    }

    public void incrementerJour(){
        modele.incrementerJour();
    }

    public void addOiseau(String nom, int age, String couleurAiles){
        modele.addOiseau(nom, age, couleurAiles);
    }

    public void supprimerOiseau(OiseauVM oiseau) {
        modele.supprimerOiseau(oiseau.getModele());
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        switch (evt.getPropertyName()){
            case ManagerOiseau.PROP_DATE_DU_JOUR: // date du jour qui change
                setDateDuJour((LocalDate) evt.getNewValue());
                break;
            case ManagerOiseau.SUPPRESSION_OISEAU: // supprimer un oiseau
                oiseauxObs.remove(((IndexedPropertyChangeEvent) evt).getIndex());
                break;
            case ManagerOiseau.AJOUT_OISEAU: // ajout d'un oiseau
                oiseauxObs.add(((IndexedPropertyChangeEvent) evt).getIndex(), new OiseauVM((Oiseau)evt.getNewValue()));
                break;
        }
    }

    public void sauver() throws IOException {
        sauveur.sauver(modele.getOiseaux(), "source.bin");
    }
}
