package modele.metier.oiseaux;

import modele.metier.etat.Affame;
import modele.metier.etat.Decede;
import modele.metier.etat.Etat;
import modele.metier.etat.Rassasie;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class Oiseau implements Serializable {

    private String nom;
    private int age;
    private String couleurAiles;
    private LocalDate dateDernierRepas;
    private Etat etat;

    private PropertyChangeSupport support = new PropertyChangeSupport(this); // transforme mon objet en beans

    public static final String PROP_ETAT = "Etat";

    public static final String PROP_DATE_DERNIER_REPAS = "DateDernierRepas";

    public Oiseau(String nom, int age, String couleurAiles, LocalDate dateDernierRepas) {
        this.nom = nom;
        this.age = age;
        this.couleurAiles = couleurAiles;
        this.dateDernierRepas = dateDernierRepas;
        this.etat = new Rassasie();
    }

    public Oiseau(String nom, int age, String couleurAiles) {
        this.nom = nom;
        this.age = age;
        this.couleurAiles = couleurAiles;
        this.dateDernierRepas = LocalDate.now();
        this.etat = new Rassasie();
    }

    public void updateEtat(LocalDate dateActuel) {
        if (ChronoUnit.DAYS.between(getDateDernierRepas(), dateActuel) == 1) {
            setEtat(new Affame());
        } else if (ChronoUnit.DAYS.between(getDateDernierRepas(), dateActuel) == 3){
            setEtat(new Decede());
        }
    }

    public void seNourrir(LocalDate date){
        if(getEtat().getClass() != Decede.class){
            setEtat(new Rassasie());
            setDateDernierRepas(date);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        if(this == obj) return true;
        if(this.getClass() != obj.getClass()) return false;

        Oiseau other = (Oiseau)obj;
        return getNom().equals(((Oiseau) obj).getNom()) && getAge() == other.getAge() && getCouleurAiles().equals(other.getCouleurAiles());
    }

    public void ajouterListener(PropertyChangeListener listener){
        support.addPropertyChangeListener(listener);
    }

    public String getNom() {
        return nom;
    }

    public int getAge() {
        return age;
    }

    public String getCouleurAiles() {
        return couleurAiles;
    }

    public LocalDate getDateDernierRepas() {
        return dateDernierRepas;
    }

    public Etat getEtat() {
        return etat;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setEtat(Etat etat){
        Etat oldValue = this.etat;
        this.etat = etat;
        support.firePropertyChange(PROP_ETAT, oldValue, this.etat); // prévient l'observateur que la propriété a changé
    }

    public void setDateDernierRepas(LocalDate dateDernierRepas){
        LocalDate oldValue = this.dateDernierRepas;
        this.dateDernierRepas = dateDernierRepas;
        support.firePropertyChange(PROP_DATE_DERNIER_REPAS, oldValue, this.dateDernierRepas);
    }
}
