package vues.uc;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import viewmodel.OiseauVM;
import vues.MainWindow;

import java.io.IOException;

public class UserControl_CelluleOiseau extends BorderPane {

    @FXML
    private Label labelEtat;

    @FXML
    private Label nomOiseau;

    @FXML
    private Button nourrirOiseau;

    private OiseauVM oiseau;

    public UserControl_CelluleOiseau(OiseauVM oiseau) throws IOException {
        this.oiseau = oiseau;
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/vues/uc/UserControl_Cellule.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        fxmlLoader.load();
    }

    private void setBackground(){
        labelEtat.setBackground(new Background(new BackgroundFill(Color.valueOf(oiseau.getEtat().getEtat()), null, null)));
    }


    @FXML
    private void nourrirOiseau(){
        oiseau.seNourrir(MainWindow.managerVM.getDateDuJour());
    }

    public void initialize() {
        nomOiseau.textProperty().bind(oiseau.nomProperty());
        setBackground();
        oiseau.etatProperty().addListener((__) -> setBackground());
    }

    public void unbind() {
        nomOiseau.textProperty().unbind();
    }
}
