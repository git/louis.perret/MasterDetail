package modele.metier.factory;

import modele.metier.oiseaux.Oiseau;

import java.time.LocalDate;

public class SimpleOiseauFactory implements OiseauFactory {

    @Override
    public Oiseau creerOiseau(String nom, int age, String couleurAiles) {
        return new Oiseau(nom, age, couleurAiles, LocalDate.now());
    }
}
