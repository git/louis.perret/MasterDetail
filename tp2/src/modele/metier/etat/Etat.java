package modele.metier.etat;

import java.io.Serializable;

public abstract class Etat implements Serializable {

    private String couleur;

    protected Etat(String couleur){
        this.couleur = couleur;
    }

    public String getEtat(){
        return couleur;
    }
}
