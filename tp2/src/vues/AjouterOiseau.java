package vues;

import javafx.fxml.FXML;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class AjouterOiseau {


    @FXML
    public TextField nomOiseau;

    @FXML
    public Spinner ageOiseau;

    @FXML
    public TextField couleurOiseau;


    @FXML
    public void ajouterOiseau() {
        MainWindow.managerVM.addOiseau(nomOiseau.getText(), (Integer)ageOiseau.getValue(), couleurOiseau.getText());
        Stage stage = (Stage)nomOiseau.getScene().getWindow();
        stage.close();
    }

    public void initialize(){
        ageOiseau.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0,20));
    }
}
