package modele.metier;

import data.chargeur.Chargeur;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import modele.metier.factory.OiseauFactory;
import modele.metier.factory.SimpleOiseauFactory;
import modele.metier.oiseaux.Oiseau;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ManagerOiseau {

    private List<Oiseau> oiseaux;

    private OiseauFactory factoryOiseau;

    private LocalDate dateDuJour;

    private Oiseau oiseauCourant;

    private int nombreDeJour = 0;

    private PropertyChangeSupport support = new PropertyChangeSupport(this); // transforme l'objet en beans

    public static final String PROP_DATE_DU_JOUR = "DateDuJour";

    public static final String SUPPRESSION_OISEAU = "Oiseau_supprime";

    public static final String AJOUT_OISEAU = "Oiseau_ajoute";

    public ManagerOiseau() {
        dateDuJour = LocalDate.now();
        oiseaux = new ArrayList<>();
        factoryOiseau = new SimpleOiseauFactory();
    }

    public void incrementerJour(){
        setDateDuJour(getDateDuJour().plusDays(1));
        verifierDateDernierRepas(getDateDuJour());
    }

    public void verifierDateDernierRepas(LocalDate dateDuJour){
        for(Oiseau oiseau : oiseaux){
            oiseau.updateEtat(dateDuJour);
        }
    }

    public void addOiseau(String nom, int age, String couleurAiles){
        Oiseau oiseau = factoryOiseau.creerOiseau(nom, age, couleurAiles);
        oiseaux.add(oiseau);
        int index = oiseaux.size() - 1; // donc l'index d'ajout est nbOiseau - 1 (évite les index out of the bound)
        support.fireIndexedPropertyChange(AJOUT_OISEAU, index, null, oiseau); // j'ajoute à la fin donc la oldValue est à null car y'avait pas d'oiseau avant à cet index là
    }

    public void addOiseau(Oiseau oiseau){
        oiseaux.add(oiseau); // j'ajoute à la fin
        int index = oiseaux.size() - 1; // donc l'index d'ajout est nbOiseau - 1 (évite les index out of the bound)
        support.fireIndexedPropertyChange(AJOUT_OISEAU, index, null, oiseau); // j'ajoute à la fin donc la oldValue est à null car y'avait pas d'oiseau avant à cet index là
    }

    public void addListener(PropertyChangeListener listener){
        support.addPropertyChangeListener(listener);
    }

    public void addOiseaux(List<Oiseau> listeOiseaux){
        oiseaux.addAll(listeOiseaux);
    }

    public void supprimerOiseau(Oiseau oiseau) {
        int index = oiseaux.indexOf(oiseau);
        oiseaux.remove(index);
        support.fireIndexedPropertyChange(SUPPRESSION_OISEAU, index, oiseau, null);
    }

    public LocalDate getDateDuJour() {
        return dateDuJour;
    }

    public void setDateDuJour(LocalDate dateDuJour) {
        LocalDate oldValue = this.dateDuJour;
        this.dateDuJour = dateDuJour;
        support.firePropertyChange(PROP_DATE_DU_JOUR, oldValue, this.dateDuJour);
    }

    public Oiseau getOiseauCourant() {
        return oiseauCourant;
    }

    public void setOiseauCourant(Oiseau oiseauCourant) {
        this.oiseauCourant = oiseauCourant;
    }

    public List<Oiseau> getOiseaux() {
        return oiseaux;
    }
}
