package data.chargeur;

import modele.metier.oiseaux.Oiseau;

import java.io.IOException;
import java.util.List;

public interface Sauveur {

    public void sauver(List<Oiseau> listeOiseaux,  String nomFichier) throws IOException;
}
