package data.chargeur;

import modele.metier.oiseaux.Oiseau;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface Chargeur {

    public List<Oiseau> charger(String nomFichier) throws IOException, ClassNotFoundException;
}
