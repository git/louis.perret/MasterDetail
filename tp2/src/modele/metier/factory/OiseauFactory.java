package modele.metier.factory;

import modele.metier.oiseaux.Oiseau;

import java.time.LocalDate;

public interface OiseauFactory {

    public Oiseau creerOiseau(String nom, int age, String couleurAiles);
}
