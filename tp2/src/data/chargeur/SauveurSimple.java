package data.chargeur;

import modele.metier.oiseaux.Oiseau;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;

public class SauveurSimple implements Sauveur{

    @Override
    public void sauver(List<Oiseau> listeOiseaux, String nomFichier) throws IOException {
        try(ObjectOutputStream writer = new ObjectOutputStream(new FileOutputStream(nomFichier))){
            writer.writeObject(listeOiseaux);
        }
    }
}
